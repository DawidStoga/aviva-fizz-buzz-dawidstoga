﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFizzBuzzApp.Controllers;
using MyFizzBuzzApp.Models;
using MyFizzBuzzApp.Services;
using MyFizzBuzzApp.Tests.Mock;

namespace MyFizzBuzzApp.Tests.Controllers
{
    [TestClass]
    public class FizzBuzzControllerTest
    {
        ITimeProvider WednesdayProvider, TimeProvider;
        public FizzBuzzControllerTest()
        {
            WednesdayProvider = new MockTimeProvider(new DateTime(2019, 07, 03));
            TimeProvider = new MockTimeProvider(new DateTime(2019, 07, 04));
        }
        [TestMethod]
        public void Play_Action_Shuould_Return_Correct_View()
        {
            //Arrange
            var mockInput = new UserInputViewModel { UserNumber = 100 };
            IFizzBuzzService service = new FizzBuzzService(TimeProvider);
            var fbController = new FizzBuzzController(service);

           //Act
            var result = fbController.Play(mockInput) as ViewResult;
            var modelview = result.Model as OutputViewModel;
            var output = modelview.FizzBuzzOutput.ToList();
            //Assert

            Assert.IsNotNull(result.Model);
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Display");
            Assert.IsInstanceOfType(result.Model, typeof(OutputViewModel));
            Assert.IsNotNull(modelview);
            Assert.AreEqual(20, output.Count);
        }
    }
}
