﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyFizzBuzzApp.Services;
using MyFizzBuzzApp.Tests.Mock;

namespace UnitTestProject1
{
    [TestClass]
    public class FizzBuzzServiceTest
    {
        ITimeProvider _mockWednesdayProvider;
        ITimeProvider _mockTimeProvider;
        public FizzBuzzServiceTest()
        {
            _mockWednesdayProvider = new MockTimeProvider(new DateTime(2019, 07, 03));
            _mockTimeProvider = new MockTimeProvider(new DateTime(2019, 07, 02));
        }
        [TestMethod]
        public void Service_Should_Return_Collection_of_20_strings_when_input_20()
        {
            //Arrange
            var userInput = 20;

            //Act
            var result = new FizzBuzzService(_mockTimeProvider).Execute(userInput);

            //Assert
            result.Should().NotBeNull();
            result.Count.Should().Be(20);
            //todo: Extend when Service is ready
        }

        [TestMethod]
        public void Service_Should_Return_Correct_Sequence_of_numbers_ending_With_fizz_When_Input_3_No_Wednesday()
        {    
            //Arrange
            var userInput = 3;
            var expected = new List<string> { "1", "2", "Fizz" };


            //todo: Extend when Service is ready
            //Act
            var result = new FizzBuzzService(_mockTimeProvider).Execute(userInput);
            //Assert
            result.Should().NotBeNull();
            result.Count.Should().Be(3);
            result.Should().BeEquivalentTo(expected);

        }
        [TestMethod]
        public void Service_Should_Return_Correct_Sequence_of_numbers_fizz_on_third_position_and_buzz_on_fifth_position_When_Input_5_and_No_Wednesday()
        {
            //Arrange
            var userInput = 5;
            var expected = new List<string> { "1", "2", "Fizz", "4", "Buzz" };


            //Act
            var result = new FizzBuzzService(_mockTimeProvider).Execute(userInput);
            //Assert
            result.Should().NotBeNull();
            result.Count.Should().Be(5);
            result.Should().BeEquivalentTo(expected);

            //todo: Extend when Service is ready
        }
        [TestMethod]
        public void Service_Should_Return_Correct_Sequence_with_Fizz_Buzz_at_the_end_When_Input_15_and_No_Wednesday()
        {
            //Arrange
            var userInput = 15;
            var expected = new List<string> { "1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz" };

            //Act
            var result = new FizzBuzzService(_mockTimeProvider).Execute(userInput);
            //Assert
            result.Should().NotBeNull();
            result.Count.Should().Be(15);
            result.Should().BeEquivalentTo(expected);
            //todo: Extend when Service is ready
        }
        [TestMethod]
        public void Service_Should_Return_Correct_Sequence_with_Wizz_Wuzz_at_the_end_When_Input_15_and_Is_Wedenesday()
        {
            
            var userInput = 15;
            var expected = new List<string> { "1", "2", "Wizz", "4", "Wuzz", "Wizz", "7", "8", "Wizz", "Wuzz", "11", "Wizz", "13", "14", "WizzWuzz" };

            //act
            var result = new FizzBuzzService(_mockWednesdayProvider).Execute(userInput);
            //arrange
            result.Count.Should().Be(15);
            result.Should().BeEquivalentTo(expected);

        }

        [TestMethod]
        public void Service_Should_Return_Exception_If_UserInput_Is_Greater_than_Thousand()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new FizzBuzzService(_mockTimeProvider).Execute(1001));

        }
        [TestMethod]
        public void Service_Should_Return_Exception_If_UserInput_is_Negative()
        {
            Action exec = () => new FizzBuzzService(_mockTimeProvider).Execute(-5);
            exec.Should().Throw<ArgumentOutOfRangeException>();

        }
        [TestMethod]
        public void Service_Should_Return_Exception_If_UserInput_is_Zero()
        {
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => new FizzBuzzService(_mockTimeProvider).Execute(0));
        }
    }
}
