﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFizzBuzzApp.Services;

namespace MyFizzBuzzApp.Tests.Mock
{
    public class MockTimeProvider : ITimeProvider
    {
        private readonly DateTime _dateTime;
        public MockTimeProvider(DateTime dateTime)
        {
            _dateTime = dateTime;
        }
        public DateTime GetDate()
        {
            return _dateTime;
        }
    }
}
