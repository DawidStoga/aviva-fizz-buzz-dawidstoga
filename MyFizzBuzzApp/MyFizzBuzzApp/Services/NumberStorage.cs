﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFizzBuzzApp.Services
{
    public interface INumberStorage
    {
        List<string> GetNumbers();
        string GetNumber(int position);
        void SaveNumbers(int number);
    }
    public class NumberStorage : INumberStorage
    {
        public void SaveNumbers(int input)
        {
           //Mocked repository for demo purpose
        }
        public List<string> GetNumbers()
        {
           var mockedResult= new List<string> { "1", "2", "Fizz" };
            return mockedResult;
        }
        public string GetNumber(int position = 1)
        {
            return "100";
        }
    }
}