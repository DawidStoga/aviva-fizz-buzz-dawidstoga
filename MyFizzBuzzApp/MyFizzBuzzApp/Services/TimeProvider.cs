﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFizzBuzzApp.Services
{
    public interface ITimeProvider
    {
        DateTime GetDate();
    }
    public class TimeProvider : ITimeProvider
    {
        public DateTime GetDate()
        {
            return DateTime.UtcNow;
        }
    }
}