﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFizzBuzzApp.Services
{

    public interface IFizzBuzzService
    {
        List<string> Execute(int inputNumber);
    }

    public class FizzBuzzService : IFizzBuzzService
    {

        protected readonly ITimeProvider _timeProvider;
        public FizzBuzzService(ITimeProvider timeProvider)
        {
            _timeProvider = timeProvider;
        }

        public List<string> Execute(int inputNumber)
        {

            if (inputNumber < 1 || inputNumber > 1000)
            {
                throw new ArgumentOutOfRangeException("Input number is out of  range : 1-1000");
            }

            return Enumerable.Range(1, inputNumber)
                            .Select(l => Transform(l))
                            .ToList();
        }

        private string Transform(int f)
        {
            DayOfWeek today = _timeProvider.GetDate().DayOfWeek;
            if (f % 3 == 0 && f % 5 == 0)
            {
                return today == DayOfWeek.Wednesday ? "WizzWuzz" : "FizzBuzz";
            }
            else if (f % 3 == 0)
            {
                return today == DayOfWeek.Wednesday ? "Wizz" : "Fizz";
            }
            else if (f % 5 == 0)
            {
                return today == DayOfWeek.Wednesday ? "Wuzz" : "Buzz";
            }
            else
            {
                return f.ToString();
            }
        }
    }
}