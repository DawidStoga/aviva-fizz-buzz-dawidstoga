﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MyFizzBuzzApp.Services;

namespace MyFizzBuzzApp.API
{
    public class FizzNumberController : ApiController
    {
        INumberStorage _storage;
        public FizzNumberController(INumberStorage storage)
        {
            _storage = storage;
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return _storage.GetNumbers();
        }

        [Route("{position}")]
        public IHttpActionResult Get(int position)
        {
            if(position<0)
            {
                return BadRequest();
            }
            return Ok(_storage.GetNumber(position));
        }

        [Route("{userInput}")]
        public IHttpActionResult Post(int userInput)
        {
            if (userInput < 1 || userInput > 100)
            {
                return BadRequest();
            }
            _storage.SaveNumbers(userInput);
            return Ok();
        }
       
    }
}