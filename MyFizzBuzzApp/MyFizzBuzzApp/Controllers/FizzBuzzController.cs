﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyFizzBuzzApp.Models;
using MyFizzBuzzApp.Services;
using PagedList;

namespace MyFizzBuzzApp.Controllers
{
    public class FizzBuzzController : Controller
    {
        protected readonly IFizzBuzzService _fizzBuzzService;
        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            _fizzBuzzService = fizzBuzzService;
        }
        public ActionResult Play()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Play(UserInputViewModel input, int page = 1)
        {
            int pageSize = 20;

            var output = new OutputViewModel();
            if (ModelState.IsValid)
            {
                var result = _fizzBuzzService.Execute(input.UserNumber);
                //todo: Moved to external service
                //result.Skip(pageSize * (page - 1)).Take(pageSize).
                output.FizzBuzzOutput = result.ToPagedList(1, pageSize);
                // todo: use mapper for complex structures.
                return View("Display", output);
            }
            return View();
        }
        [HttpGet]
        public ActionResult PlayPage(int userInput, int page = 1)
        {
            int pageSize = 20;
            var output = new OutputViewModel();
            var result = _fizzBuzzService.Execute(userInput);
            output.FizzBuzzOutput = result.ToPagedList(page, pageSize);
            return View("Display", output);
        }
    }
}