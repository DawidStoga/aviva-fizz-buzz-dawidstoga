using System.Web.Http;
using WebActivatorEx;
using MyFizzBuzzApp;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace MyFizzBuzzApp
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                    {
                        c.SingleApiVersion("v1", "MyFizzBuzzApp");
                        c.PrettyPrint();
                    })
                .EnableSwaggerUi();
        }
    }
}
