﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyFizzBuzzApp.Models
{
    public class OutputViewModel
    {
        public IEnumerable<string> FizzBuzzOutput { get; set; }
    }
}