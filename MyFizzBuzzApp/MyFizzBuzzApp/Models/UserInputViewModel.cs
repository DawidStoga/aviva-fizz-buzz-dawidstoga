﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyFizzBuzzApp.Models
{
    public class UserInputViewModel
    {
        [Required]
        [Range(1, 1000)]
        public int UserNumber { get; set; }
    }
}